const jwt = require('jsonwebtoken');
const userService = require('../services/userService');

module.exports = async (req, res, next) => {
    try {
        const token = req.headers.authorization;
        const decodedToken = jwt.verify(token, 'shhhhh');
        const maybeTokenUser = await userService.findById(decodedToken._id);

        if (!maybeTokenUser) {
            return res.status(500).json({
                error: 'Could not find user from token'
            });
        }

        if (!userService.hasRole(maybeTokenUser, 'ADMIN')) {
            return res.status(500).json({
                error: 'User is not authorized to perform this action'
            });
        } else {
            next();
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
};
