const database = require('../database');
const User = require('../models/User');
const UserSettings = require('../models/UserSettings');
const bcrypt = require('bcrypt');
const seed = require('./seed');

exports.seedDatabase = async (req, res) => {
    try {
        await database.connect();

        await this.createUsers();
    } catch (e) {
        console.error(e);
    }
}

exports.createUsers = async () => {

    const users = [
        {
            username: 'guest',
            email: 'guest@res-rel.fr',
            firstName: 'Jean',
            lastName: 'GUEST',
            role: 'GUEST',
            password: await bcrypt.hash('guest', 10),
            isActivated: true
        },
        {
            username: 'user',
            email: 'user@res-rel.fr',
            firstName: 'Jean',
            lastName: 'USER',
            role: 'USER',
            password: await bcrypt.hash('user', 10),
            isActivated: true
        },
        {
            username: 'moderator',
            email: 'moderator@res-rel.fr',
            firstName: 'Jean',
            lastName: 'MODERATOR',
            role: 'MODERATOR',
            password: await bcrypt.hash('moderator', 10),
            isActivated: true
        },
        {
            username: 'admin',
            email: 'admin@res-rel.fr',
            firstName: 'Jean',
            lastName: 'ADMIN',
            role: 'ADMIN',
            password: await bcrypt.hash('admin', 10),
            isActivated: true
        },
        {
            username: 'super-admin',
            email: 'super-admin@res-rel.fr',
            firstName: 'Jean',
            lastName: 'SUPERADMIN',
            role: 'SUPERADMIN',
            password: await bcrypt.hash('superadmin', 10),
            isActivated: true
        },
        {
            username: 'user-001',
            email: 'user-001@res-rel.fr',
            firstName: 'Jean',
            lastName: 'USER-001',
            role: 'USER',
            password: await bcrypt.hash('user', 10),
            isActivated: true
        },
        {
            username: 'user-002',
            email: 'user-002@res-rel.fr',
            firstName: 'Jean',
            lastName: 'USER-002',
            role: 'USER',
            password: await bcrypt.hash('user', 10),
            isActivated: true
        },
        {
            username: 'user-003',
            email: 'user-003@res-rel.fr',
            firstName: 'Jean',
            lastName: 'USER-003',
            role: 'USER',
            password: await bcrypt.hash('user', 10),
            isActivated: true
        },
        {
            username: 'user-004',
            email: 'user-004@res-rel.fr',
            firstName: 'Jean',
            lastName: 'USER-004',
            role: 'USER',
            password: await bcrypt.hash('user', 10),
            isActivated: true
        },
        {
            username: 'user-005',
            email: 'user-005@res-rel.fr',
            firstName: 'Jean',
            lastName: 'USER-005',
            role: 'USER',
            password: await bcrypt.hash('USER', 10),
            isActivated: true
        },
    ]

    for (let user of users) {
        let createdUserId;
        await User.findOneAndUpdate(
            {
                username: user.username,
                email: user.email
            },
            user,
            {
                upsert: true,
                setDefaultsOnInsert: true,
                new: true
            },
            (err, doc) => {
                if (err) {
                    throw err;
                }
                createdUserId = doc._id;
            }
        )

        const userSettings = await UserSettings.findOneAndUpdate(
            {user: createdUserId},
            {},
            {
                upsert: true,
                setDefaultsOnInsert: true,
                new: true
            },
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            }
        );

        await User.findOneAndUpdate(
            {_id: createdUserId},
            {userSettings: userSettings._id},
            {},
            (err, doc) => {
                if (err) {
                    throw err;
                }
            }
        )
    }
}

exports.createRelations = async () => {

}

module.exports = seed;
