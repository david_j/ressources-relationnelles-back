const assert = require('assert');
const mongoose = require("mongoose");
const User = require('../models/User');

const database = 'mongo:27017';
// const database = 'localhost:27017';

describe('Test Connexion DB', async function () {
    describe('#connectToDB()', async function () {
        it('should connect to test db', async function () {
            const connect = await mongoose.createConnection(`mongodb://${database}/test`);
            assert.strictEqual(connect._readyState, 1);
        });
    });
})

describe('Test Create User', async function () {
    describe('#createUser()', async function () {
        it('should create user', async function () {
            const connect = await mongoose.connect(`mongodb://${database}/test`);
            const user = await User.create(
                {
                    email: 'user-test-001@res-rel.fr',
                    username: 'UserTest001',
                    firstName: 'User',
                    lastName: 'Test-001',
                }
            )
                .then(doc => {
                    return doc;
                })
                .catch(err => {
                    console.error(err);
                })
            assert.notStrictEqual(user, null);
        });
    });
})

describe('Test Delete User', async function () {
    describe('#createUser()', async function () {
        it('should create user', async function () {
            const connect = await mongoose.connect(`mongodb://${database}/test`);
            const deleteRes = await User.deleteOne(
                {
                    email: 'user-test-001@res-rel.fr',
                    username: 'UserTest001',
                    firstName: 'User',
                    lastName: 'Test-001',
                }
            )
                .then(doc => {
                    return doc;
                })
                .catch(err => {
                    console.error(err);
                })
            assert.strictEqual(deleteRes.ok, 1);
        });
    });
})


describe('Test Create And Update User', async function () {
    describe('#createUser()', async function () {
        it('should create and update user', async function () {
            const connect = await mongoose.connect(`mongodb://${database}/test`);
            const user = await User.create(
                {
                    email: 'user-test-001@res-rel.fr',
                    username: 'UserTest001',
                    firstName: 'User',
                    lastName: 'Test-001',
                }
            )
                .then(doc => {
                    return doc;
                })
                .catch(err => {
                    console.error(err);
                })
            const updatedUser = await User.findOneAndUpdate(
                {
                    email: 'user-test-001@res-rel.fr',
                    username: 'UserTest001',
                    firstName: 'User',
                    lastName: 'Test-001',
                },
                {
                    email: 'user-test-001@res-rel.fr__UPDATED',
                    username: 'UserTest001__UPDATED',
                    firstName: 'User__UPDATED',
                    lastName: 'Test-001__UPDATED',
                }
            )
            assert.notStrictEqual(updatedUser, null);
        });
    });
})
