const EventResponseEnum = Object.freeze({
        UNSURE: 0,
        ACCEPTED: 1,
        REFUSED: 2,
    }
)

const EventResponsesNamesFromEnum = {
    UNSURE: 'UNSURE',
    ACCEPTED: 'ACCEPTED',
    REFUSED: 'REFUSED'
}

const EventResponsesNamesFromAnswer = {
    UNSURE: 'UNSURE',
    ACCEPT: 'ACCEPTED',
    REFUSE: 'REFUSED'
}

module.exports = {
    EventResponseEnum,
    EventResponsesNamesFromEnum,
    EventResponsesNamesFromAnswer
};
