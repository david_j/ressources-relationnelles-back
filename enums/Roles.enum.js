const RolesEnum = Object.freeze({
        'GUEST': 0,
        'USER': 1,
        'MODERATOR': 2,
        'ADMIN': 3,
        'SUPERADMIN': 4
    }
)

const RolesNamesFromEnum = {
    GUEST: 'GUEST',
    USER: 'USER',
    MODERATOR: 'MODERATOR',
    ADMIN: 'ADMIN',
    SUPERADMIN: 'SUPERADMIN',
}

const RolesValuesFromEnum = {
    GUEST: 0,
    USER: 1,
    MODERATOR: 5,
    ADMIN: 10,
    SUPERADMIN: 100,
}

module.exports = {
    RolesEnum,
    RolesNamesFromEnum,
    RolesValuesFromEnum,
};
