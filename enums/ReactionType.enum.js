const ReactionTypeEnum = Object.freeze({
        LIKE: 0,
        DISLIKE: 1,
        LOVE: 2,
        INTERESTED: 3,
        ANGRY: 4
    }
)

module.exports = ReactionTypeEnum;
