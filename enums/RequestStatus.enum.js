const RequestStatusEnum = Object.freeze({
    'PENDING': 0,
    'ACCEPTED': 1,
    'REFUSED': 2,
});

const RequestStatusEnumNamesFromEnum = {
    PENDING: 'PENDING',
    ACCEPTED: 'ACCEPTED',
    REFUSED: 'REFUSED'
}

module.exports = {
    RequestStatusEnum,
    RequestStatusEnumNamesFromEnum
};
