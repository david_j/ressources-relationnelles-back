# Ressources Relationnelles

---

## Description

> **Ressources Relationnelles est un réseau social basé sur le partage et la communication entre les individus.**

## Technologies utilisées

* **Angular (web)**
* **Express (serveur)**
* **Flutter (mobile)**

## Démarrer le projet

* ### **Front**

`$ npm install`

`$ npm start`

* ### **Back**

`$ nodemon server`

* ### **Mobile**

`$ flutter run lib/main.dart`

---

**Créé par :**

* Dimitry 🦕
* Jérôme 🦆
* Pierre 🦓
* Tom 🐧
