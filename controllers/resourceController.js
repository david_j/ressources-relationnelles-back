let database = require('../database');
let Resource = require('../models/Resource');
let resourceService = require('../services/resourceService');

// FIND ALL RESOURCES FOR CURRENT USER
exports.findAllByCurrentUser = async (req, res) => {
    try {
        const maybeResourcesByCurrentUser = await resourceService.findAllByCurrentUser(req);

        if (maybeResourcesByCurrentUser) {
            return res.status(200).send(maybeResourcesByCurrentUser);
        } else {
            return res.status(500).send('An error occurred while trying to find all resources by current user');
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// FIND ALL RESOURCES FOR CURRENT USER
exports.findAllByCurrentUserRelations = async (req, res) => {
    try {
        const maybeResourcesByCurrentUser = await resourceService.findAllByCurrentUserRelations(req);

        if (maybeResourcesByCurrentUser) {
            return res.status(200).send(maybeResourcesByCurrentUser);
        } else {
            return res.status(500).send('An error occurred while trying to find all resources by current user');
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// FIND RELATIONS FEED BY CURRENT USER
exports.findRelationsFeedByCurrentUser = async (req, res) => {
    try {
        const maybeResourcesFeedByCurrentUser = await resourceService.findResourcesFeedByCurrentUser(req);

        if (maybeResourcesFeedByCurrentUser) {
            return res.status(200).send(maybeResourcesFeedByCurrentUser);
        } else {
            return res.status(500).send('An error occurred while trying to find resources feed by current user');
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// FIND RESOURCE BY ID
exports.findResourceById = async (req, res) => {
    try {
        const id = req.params.id;
        return res.send('Finding resource ' + id + '...');
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// CREATE RESOURCE
exports.create = async (req, res) => {
    try {
        const resource = JSON.parse(req.body.resource);
        let maybeImage;
        if (req.files) {
            maybeImage = req.files.image;
        }
        const newResource = await resourceService.create(req, resource, maybeImage);

        if (newResource) {
            return res.status(200).send(newResource);
        } else {
            return res.status(500).send('Could not save new resource');
        }
    } catch (e) {
        console.error(e);
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.sendResourceMessage = async (req, res) => {
    try {
        const maybeSavedMessage = await resourceService.sendMessage(req, res);
        if (!maybeSavedMessage) {
            return res.status(500).send('An error occurred while trying to send message');
        }

        return res.status(201).json({
            message: 'Successfully sent message',
            res: maybeSavedMessage
        });

    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// UPDATE RESOURCE TODO: Add method
exports.update = async (req, res) => {
    try {
        const resource = JSON.parse(req.body.resource);
        // console.log(resource);
        let maybeImage;
        if (req.files) {
            maybeImage = req.files.image;
        }

        const updatedResource = await resourceService.updateResource(req, resource, maybeImage);
        if (!updatedResource) {
            return res.status(500).send('Could not update resource');
        }
        return res.status(200).send(updatedResource);
    } catch (e) {

    }
}

// DELETE RESOURCE TODO: Add method
exports.delete = async (req, res) => {
    try {
        const maybeDeletedResource = await resourceService.deleteResource(req);
        if (!maybeDeletedResource) {
            return res.status(500).send('An error occurred while trying to delete resource');
        }
        return res.status(200).send(maybeDeletedResource);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.uploadResourceImage = async (req, res) => {
    try {
        if (!req.files) {
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            const image = req.files.image;
            const maybeUploadedImage = await resourceService.uploadResourceImage(req, image);
            if (!maybeUploadedImage) {
                return res.status(500).send('Could not save uploaded image');
            }

            return res.status(201).send('Successfully uploaded image');
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.answerEvent = async (req, res) => {
    try {
        const maybeAnsweredEvent = await resourceService.answerEvent(req);

        if (!maybeAnsweredEvent) {
            return res.status(500).send('Could not answer event');
        }

        return res.status(201).send(maybeAnsweredEvent);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.likeResource = async (req, res) => {
    try {
        const maybeLikedResource = await resourceService.likeResource(req);
        if (!maybeLikedResource) {
            return res.status(500).send('Could not like resource');
        }
        return res.status(201).send(maybeLikedResource);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.unlikeResource = async (req, res) => {
    try {
        const maybeUnlikedResource = await resourceService.unlikeResource(req);
        if (!maybeUnlikedResource) {
            return res.status(500).send('Could not unlike resource');
        }
        return res.status(201).send(maybeUnlikedResource);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}
