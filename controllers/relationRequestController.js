let database = require('../database');
let RelationRequest = require('../models/RelationRequest');
let relationRequestService = require('../services/relationRequestService');
const relationService = require('../services/relationService');
const userService = require('../services/userService');

// CLASSIC METHODS

// FIND ALL BY CURRENT USER

exports.findAllByCurrentUser = async (req, res) => {
    try {
        const maybeRequestList = await relationRequestService.findAllByCurrentUser(req, res);

        if (maybeRequestList) {
            return res.status(200).send(maybeRequestList);
        } else {
            return res.status(401).send('Could not find relation request list');
        }

    } catch (e) {
        return res.status(500).json({
            error: e.message
        })
    }
}

exports.sendRelationRequest = async (req, res) => {
    try {
        const relationRequest = new RelationRequest(req.body);
        const currentUser = await userService.getCurrent(req, res);

        if (!currentUser) {
            return res.status(401).send('Could not find current user');
        }
        relationRequest.fromUserId = currentUser._id;
        const maybeExistingRelationRequest = await RelationRequest.find(
            {
                fromUserId: currentUser._id,
                toUserId: relationRequest.toUser._id
            }
        )

        if (maybeExistingRelationRequest) {
            return res.status(400).send('A relation request was already sent to this user');
        }

        const savedRelationRequest = await relationRequest.save();

        if (savedRelationRequest) {
            return res.status(200).send('Successfully sent relation request');
        } else {
            return res.status(500).json({
                status: 500,
                error: 'An error occurred while trying to send relation request'
            })
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.answerRelationRequest = async (req, res) => {
    try {
        const accepterAnswers = ['accept', 'refuse'];
        const relationRequestId = req.params.id;
        const relationRequestAnswer = req.params.answer;

        if (!accepterAnswers.includes(relationRequestAnswer)) {
            return res.status(401).send('Unallowed anwser to relation request');
        }

        const maybeAnsweredRelationRequest = await relationRequestService.answerRelationRequest(req, relationRequestId, relationRequestAnswer);
        if (!maybeAnsweredRelationRequest) {
            return res.status(500).send('Could not answer relation request');
        }

        if (relationRequestAnswer === 'accept') {
            const maybeCreatedRelation = await relationService.createRelationsFromAcceptedRequest(req, maybeAnsweredRelationRequest);
            if (!maybeCreatedRelation) {
                return res.status(500).send('Could not create relation from acceptedRequest');
            }
            return res.status(201).json({
                message: `Successfully accepted relation request`,
                doc: maybeCreatedRelation
            });
        } else {
            return res.status(200).json({
                message: `Successfully refused relation request`,
                doc: maybeAnsweredRelationRequest
            });
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.cancelRelationRequest = async (req, res) => {
    try {
        const relationRequestId = req.params.id;
        const maybeCancelledRelationRequest = await relationRequestService.cancelRequest(req, relationRequestId);
        if (!maybeCancelledRelationRequest) {
            return res.status(500).send('Could not cancel relation request');
        }

        return res.status(200).send(maybeCancelledRelationRequest);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.checkNotifications = async (req, res) => {
    try {
        const maybeNRequestNotifications = await relationRequestService.checkNotifications(req);
        if (!maybeNRequestNotifications) {
            return res.status(500).send('Could not find request notifications');
        }

        return res.status(200).send(maybeNRequestNotifications);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// CRUD //

// FIND RESOURCE BY ID
exports.findById = async (req, res) => {
    try {
        const id = req.params.id;
        return res.send('Finding relation request ' + id + '...');
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// CREATE RESOURCE

// UPDATE RESOURCE

// DELETE RESOURCE

// END OF CLASSIC METHODS

// ADMIN METHODS

// FIND ALL RELATIONS
exports.findAll = async (req, res) => {
    try {
        RelationRequest.find(
            (error, response) => {
                if (response) {
                    return res.status(200).send(response);
                } else {
                    console.error(error);
                    return res.status(500).send(error);
                }
            });
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// CREATE A RELATION REQUEST
exports.create = async (req, res) => {
    try {
        await database.connect();

        const newRelationRequest = new RelationRequest(req.body);
        newRelationRequest.createdAt = Date.now();

        return newRelationRequest.save()
            .then(item => {
                return res.status(201).json({
                    status: 201,
                    message: 'Relation request created successfully'
                })
            })
            .catch(err => {
                return res.status(400).send("Unable to save to database");
            });

    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// END OF ADMIN METHODS //
