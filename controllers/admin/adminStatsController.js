const adminStatsService = require('../../services/admin/adminStatsService');

exports.getDashboardStats = async (req, res) => {
    const maybeDashboardStats = await adminStatsService.getDashboardStats(req);

    if (!maybeDashboardStats) {
        return res.status(500).send('Could not get dashboard stats');
    }

    return res.status(200).send(maybeDashboardStats);
}
