let database = require('../../database');
let Resource = require('../../models/Resource');
let adminResourceService = require('../../services/admin/adminResourceService');

// FIND ALL RESOURCES
exports.findAll = async (req, res) => {
    try {
        Resource.find()
            .populate('user')
            .then(docs => {
                return res.status(200).send(docs);
            })
            .catch(e => {
                throw e;
            })
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// CREATE A RESOURCE
exports.create = async (req, res) => {
    try {
        const maybeCreatedResource = await adminResourceService.create(req);

        if (!maybeCreatedResource) {
            res.status(500).send('Could not create resource');
        }

        res.status(200).send(maybeCreatedResource);

    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// UPDATE A RESOURCE
exports.update = async (req, res) => {
    try {
        const maybeUpdatedResource = await adminResourceService.update(req);

        if (!maybeUpdatedResource) {
            return res.status(500).send('Could not update resource');
        }
        return res.status(200).send(maybeUpdatedResource);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// DELETE A RESOURCE
exports.delete = async (req, res) => {
    try {
        const deletedResource = await adminResourceService.delete(req);

        if (!deletedResource) {
            return res.status(500).send('Could not delete resource');
        }
        return res.status(200).send(deletedResource);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.ban = async (req, res) => {
    try {
        const bannedResource = await adminResourceService.ban(req);

        if (!bannedResource) {
            return res.status(500).send('Could not ban resource');
        }

        return res.status(200).send(bannedResource);
    } catch (e) {
        console.error(e);
        return res.status(500).json({
            error: e.message
        })
    }
}
