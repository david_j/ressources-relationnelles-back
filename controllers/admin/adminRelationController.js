let database = require('../../database');
let Relation = require('../../models/Relation');
let adminRelationService = require('../../services/admin/adminRelationService');

// FIND ALL RELATIONS
exports.findAll = async (req, res) => {
    try {
        const maybeAllRelations = await adminRelationService.findAll(req);

        if (!maybeAllRelations) {
            return res.status(500).send('Could not find all relations');
        }

        return res.status(200).send(maybeAllRelations);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// CREATE A RELATION
exports.create = async (req, res) => {
    try {
        const maybeCreatedRelation = await adminRelationService.create(req);

        if (!maybeCreatedRelation) {
            res.status(500).send('Could not create relation');
        }

        res.status(200).send(maybeCreatedRelation);

    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// UPDATE A RELATION
exports.update = async (req, res) => {
    try {
        const maybeUpdatedRelation = await adminRelationService.update(req);

        if (!maybeUpdatedRelation) {
            return res.status(500).send('Could not update relation');
        }
        return res.status(200).send(maybeUpdatedRelation);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// DELETE A RELATION
exports.delete = async (req, res) => {
    try {
        const deletedRelation = await adminRelationService.delete(req);

        if (!deletedRelation) {
            return res.status(500).send('Could not delete relation');
        }
        return res.status(200).send(deletedRelation);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}
