const adminChatService = require('../../services/admin/adminChatService');

exports.findAll = async (req, res) => {
    try {
        const maybeAllMessages = await adminChatService.findAll();
        if (!maybeAllMessages) {
            return res.status(500).send('Could not find all messages');
        }
        return res.status(200).send(maybeAllMessages);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        })
    }
}

exports.sendChatMessageToUser = async (req, res) => {
    try {
        const maybeSentMessage = await adminChatService.sendChatMessageToUser(req);
        if (!maybeSentMessage) {
            return res.status(500).send('Could not send message to user');
        }
        return res.status(201).send(maybeSentMessage);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        })
    }
}
