let database = require('../../database');
let RelationRequest = require('../../models/RelationRequest');
let adminRelationRequestService = require('../../services/admin/adminRelationRequestService');
const userService = require('../../services/userService');


// FIND ALL RELATIONS
exports.findAll = async (req, res) => {
    try {
        const relationRequests = await adminRelationRequestService.findAll(req);
        if (!relationRequests) {
            return res.status(500).send('Could not find relation requests');
        }
        return res.status(200).send(relationRequests);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// CREATE A RELATION REQUEST
exports.create = async (req, res) => {
    try {
        const newRelationRequest = new RelationRequest(req.body);
        newRelationRequest.createdAt = Date.now();

        return newRelationRequest.save()
            .then(item => {
                return res.status(201).json({
                    status: 201,
                    message: 'Relation request created successfully'
                })
            })
            .catch(err => {
                return res.status(400).send("Unable to save to database");
            });

    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// DELETE A RELATION REQUEST
exports.delete = async (req, res) => {
    try {
        const relationRequestId = req.params.id;

        return RelationRequest.findOneAndUpdate(
            {_id: relationRequestId},
            {isDeleted: true, deletedAt: Date.now()}
        )
            .then(item => {
                return res.status(201).json({
                    status: 201,
                    message: 'Relation request created successfully'
                })
            })
            .catch(err => {
                return res.status(400).send("Unable to save to database");
            });

    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}
