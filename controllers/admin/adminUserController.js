let database = require('../../database');
let User = require('../../models/User');
const adminUserService = require('../../services/admin/adminUserService')
const RolesEnum = require('../../enums/Roles.enum')
const bcrypt = require('bcrypt');


// FIND ALL USERS
exports.findAll = async (req, res) => {
    try {
        const maybeAllUsers = await adminUserService.findAll(req);

        if (!maybeAllUsers) {
            return res.status(500).send('Could not find all users');
        }

        return res.status(200).send(maybeAllUsers);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.findById = async (req, res) => {
    try {
        const userId = req.params.id;
        const maybeUser = await adminUserService.findById(userId);

        if (!maybeUser) {
            return res.status(404).send('Could not find user with id ' + userId);
        }

        return res.status(200).send(maybeUser);
    } catch (e) {
        return res.status(500).send({
            error: e.message
        })
    }
}

// CREATE A USER
exports.create = async (req, res) => {
    try {
        const maybeCreatedUser = await adminUserService.create(req);

        if (!maybeCreatedUser) {
            return res.status(500).send('Could not create user');
        }

        return res.status(200).send(maybeCreatedUser);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// UPDATE A USER
exports.update = async (req, res) => {
    try {
        const maybeUpdatedUser = await adminUserService.update(req);

        if (!maybeUpdatedUser) {
            return res.status(500).send('Could not update user');
        }
        return res.status(200).send(maybeUpdatedUser);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// DELETE A USER
exports.delete = async (req, res) => {
    try {
        const deletedUser = await adminUserService.delete(req);

        if (!deletedUser) {
            return res.status(500).send('Could not delete user');
        }
        return res.status(200).send(deletedUser);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.ban = async (req, res) => {
    try {
        const bannedUser = await adminUserService.ban(req);

        if (!bannedUser) {
            return res.status(500).send('Could not ban user');
        }

        return res.status(200).send(bannedUser);
    } catch (e) {
        console.error(e);
        return res.status(500).json({
            error: e.message
        })
    }
}
