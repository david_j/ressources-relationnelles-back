let relationService = require('../services/relationService');
let relationRequestService = require('../services/relationRequestService');

exports.create = async (req, res) => {
    try {
        const newRelation = await relationService.create(req, req.body);

        if (newRelation) {
            return res.status(200).send(newRelation);
        } else {
            return res.status(500).send('Could not save new resource');
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.findById = async (req, res) => {
    try {
        const relationList = await relationService.findById(req);

        return res.send('Finding relation ' + relationList);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.findAllByCurrentUser = async (req, res) => {
    try {
        const relationList = await relationService.findAllByCurrentUser(req);

        if (relationList) {
            return res.status(200).send(relationList);
        } else {
            return res.status(200).send([]);
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.update = async (req, res) => {
    try {
        const updateRelation = await relationService.update(req, req.body);

        if (updateRelation) {
            return res.status(200).send(updateRelation);
        } else {
            return res.status(500).send('Could not update relation');
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.delete = async (req, res) => {
    try {
        const deleteRelation = await relationService.delete(req, req.body);

        if (deleteRelation) {
            return res.status(200).send(deleteRelation);
        } else {
            return res.status(500).send('Could not delete relation');
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.getSuggestions = async (req, res) => {
    try {
        const relationList = await relationService.getSuggestions(req);

        if (relationList) {
            return res.status(200).send(relationList);
        } else {
            return res.status(500).send('Could not get suggesting relations');
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.findAllRelationRequestsByCurrentUser = async (req, res) => {
    try {
        const requestList = await relationRequestService.findAllByCurrentUser(req);
        if (requestList) {
            return res.status(200).send(requestList);
        }
        return res.status(500).send('Could not find relation requests by current user');
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.findRelationRequestsByStatusByCurrentUser = async (req, res) => {
    try {
        const requestList = await relationRequestService.findRelationRequestsByStatusByCurrentUser(req, req.params.status);
        if (requestList) {
            return res.status(200).send(requestList);
        }
        return res.status(500).send('Could not find relation requests by current user');
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.sendRelationRequest = async (req, res) => {
    try {
        const sentRequest = await relationRequestService.sendRelationRequest(req, req.params.userId);
        if (sentRequest) {
            return res.status(201).send(sentRequest);
        }
        return res.status(500).send('Could not send relation request');
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.deleteByRelationUser = async (req, res) => {
    try {
        const relationUserId = req.params.relationUserId;

        const maybeDeletedRelations = await relationService.deleteRelationsByRelationUser(req, relationUserId);
        if (!maybeDeletedRelations) {
            return res.status(500).send('Could not delete relation');
        }

        return res.status(200).json({
            message: `Successfully deleted relation`
        })
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.isInCurrentUserRelationList = async (req, res) => {
    try {
        const isInCurrentUserRelationList = await relationService.isInCurrentUserRelationList(req);
        return res.status(200).send(isInCurrentUserRelationList != null);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}
