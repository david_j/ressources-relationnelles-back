let User = require('../models/User');
const userService = require('../services/userService')
const RolesEnum = require('../enums/Roles.enum')


exports.findAll = async (req, res) => {
    try {
        let userSuggestionList = await User.find(
            {
                role: RolesEnum.RolesNamesFromEnum.USER,
                isBanned: false,
                isDeleted: false,
            },
        )
            .limit(10)
            .sort('email')

        const userSuggestionListDTO = userSuggestionList.map(user => user.toDTO());

        return res.status(200).send(userSuggestionListDTO);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// GET CURRENT USER
exports.getCurrent = async (req, res) => {
    try {
        const maybeCurrentUser = await userService.getCurrent(req);

        if (maybeCurrentUser) {
            return res.status(200).send(maybeCurrentUser.toDTO());
        } else {
            return res.status(404).send('Could not find current user');
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// UPDATE CURRENT USER
exports.updateCurrent = async (req, res) => {
    try {
        const maybeUpdatedUser = await userService.updateCurrent(req);

        if (maybeUpdatedUser) {
            return res.status(200).send(maybeUpdatedUser.toDTO());
        } else {
            return res.status(404).send('Could not update current user');
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// FIND USER BY ID
exports.findById = async (req, res) => {
    try {
        const id = req.params.id;
        const maybeUser = await User.findById(id);

        if (maybeUser) {
            return res.status(200).send(maybeUser);
        } else {
            return res.status(404).send('Could not find user with id : ', id);
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// FIND USER BY TEXT SEARCH
exports.findByTextSearch = async (req, res) => {
    try {
        const textSearch = req.body.textSearch;

        const maybeUser = await userService.findByTextSearch(req, textSearch);
        if (!maybeUser) {
            return res.status(500).send('Could not find user with id : ', id);
        }

        return res.status(200).send(maybeUser);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.uploadProfilePicture = async (req, res) => {
    try {
        if (!req.files) {
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            let image = req.files.image;

            const currentUser = await userService.getCurrent(req);

            //Use the mv() method to place the file in upload directory (i.e. "uploads")
            await image.mv('./uploads/public/images/' + currentUser._id + '/' + image.name);

            await User.findOneAndUpdate({_id: currentUser._id}, {profilePicture: image.name})
                .then(doc => {
                    return res.status(201).json({
                        doc
                    });
                })
                .catch(err => {
                    return res.status(500).send(err);
                })
        }
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.findSettingsForCurrentUser = async (req, res) => {
    try {
        const maybeUserSettings = await userService.findSettingsForCurrentUser(req);

        if (!maybeUserSettings) {
            return res.status(500).send('Could not find user settings');
        }
        return res.status(200).send(maybeUserSettings);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.updateSettingsForCurrentUser = async (req, res) => {
    try {
        const maybeUpdatedUserSettings = await userService.updateSettingsForCurrentUser(req);

        if (!maybeUpdatedUserSettings) {
            return res.status(500).send('Could not update user settings');
        }

        return res.status(200).send(maybeUpdatedUserSettings);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}
