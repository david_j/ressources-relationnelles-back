let RegisterUser = require('../dtos/RegisterUserDTO');
let User = require('../models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const userService = require("../services/userService");
const RolesNamesFromEnum = require("../enums/Roles.enum").RolesNamesFromEnum;
const authenticationService = require('../services/authenticationService');

// REGISTER
exports.register = async (req, res) => {
    try {
        const maybeRegisterUser = await authenticationService.register(req);

        if (!maybeRegisterUser) {
            return res.status(500).send('Could not register user');
        }
        return res.status(201).json({
            message: 'Successfully registered user',
            doc: maybeRegisterUser
        });
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

// ACTIVATION
exports.activate = async (req, res) => {
    try {
        const maybeActivatedUser = await authenticationService.activate(req);

        if (!maybeActivatedUser) {
            return res.status(500).send('Could not activate account');
        }

        return res.status(200).json({
            message: 'Successfully activated account',
            doc: maybeActivatedUser
        });
    } catch (e) {
        return res.status(500).json({
            error: e.message
        })
    }
}

// LOGIN
exports.login = async (req, res) => {
    try {
        const maybeLoginToken = await authenticationService.login(req);
        if (!maybeLoginToken) {
            return res.send(500).send('Could not login');
        }

        return res.status(200).json({token: maybeLoginToken});
    } catch (e) {
        switch (e.message) {
            case 'Incorrect password':
                return res.status(400).send(e.message);
            case 'User is not activated':
                return res.status(401).send(e.message);
            case 'User is banned':
                return res.status(401).send(e.message);
            case 'User was deleted':
                return res.status(404).send(e.message);
            case 'Email does not exist':
                return res.status(404).send(e.message);
            default:
                return res.status(500).send('An error occurred');
        }
    }
}

// VERIFY TOKEN
exports.verifyToken = (req, res) => {
    try {
        const token = req.body.token;

        jwt.verify(token, 'shhhhh', (err, decoded) => {

            if (err) {
                console.error(err);
                return res.status(400).json({
                    message: 'An error occurred while verifying the token',
                    error: err
                });
            }

            return res.status(200).send(decoded);
        });
    } catch (e) {
        return res.status(500).json({
            error: e.message
        });
    }
}

exports.isUser = async (req, res) => {
    try {
        const token = req.headers.authorization;
        const decodedToken = jwt.verify(token, 'shhhhh');
        const maybeTokenUser = await userService.findById(decodedToken._id);

        if (maybeTokenUser) {
            const hasUserRole = userService.hasRole(maybeTokenUser, RolesNamesFromEnum.USER);
            res.status(200).send(hasUserRole);
        } else {
            res.status(404).send('No user found');
        }
    } catch (e) {
        res.status(500).json({
            error: e.message
        })
    }
}

exports.isAdmin = async (req, res) => {
    try {
        const token = req.headers.authorization;
        const decodedToken = jwt.verify(token, 'shhhhh');
        const maybeTokenUser = await userService.findById(decodedToken._id);

        if (maybeTokenUser) {
            const hasAdminRole = userService.hasRole(maybeTokenUser, RolesNamesFromEnum.ADMIN);
            res.status(200).send(hasAdminRole);
        } else {
            res.status(404).send('No user found');
        }
    } catch (e) {
        res.status(500).json({
            error: e.message
        })
    }
}

exports.isSuperAdmin = async (req, res) => {
    try {
        const token = req.headers.authorization;
        const decodedToken = jwt.verify(token, 'shhhhh');
        const maybeTokenUser = await userService.findById(decodedToken._id);

        if (maybeTokenUser) {
            const hasSuperAdminRole = userService.hasRole(maybeTokenUser, RolesNamesFromEnum.SUPERADMIN);
            res.status(200).send(hasSuperAdminRole);
        } else {
            res.status(404).send('No user found');
        }
    } catch (e) {
        res.status(500).json({
            error: e.message
        })
    }
}
