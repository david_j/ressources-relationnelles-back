let database = require('../database');
const ChatMessage = require('../models/ChatMessage');
const chatService = require('../services/chatService');
const userService = require('../services/userService');

// CLASSIC METHODS
exports.findAllByCurrentUser = async (req, res) => {
    try {
        const maybeChatMessageListByCurrentUser = await chatService.findAllByCurrentUser(req);
        if (!maybeChatMessageListByCurrentUser) {
            return res.status(500).send('Could not find chat messages by current user');
        }
        return res.status(200).send(maybeChatMessageListByCurrentUser);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        })
    }
}

exports.findAllHelpMessagesByCurrentUser = async (req, res) => {
    try {
        const maybeChatHelpMessageListByCurrentUser = await chatService.findAllHelpMessagesByCurrentUser(req);
        if (!maybeChatHelpMessageListByCurrentUser) {
            return res.status(500).send('Could not find chat messages by current user');
        }
        return res.status(200).send(maybeChatHelpMessageListByCurrentUser);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        })
    }
}

exports.sendChatMessage = async (req, res) => {
    try {
        const maybeSentChatMessage = await chatService.sendChatMessage(req);
        if (!maybeSentChatMessage) {
            return res.status(500).send('Could not send chat message');
        }
        return res.status(201).send(maybeSentChatMessage);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        })
    }
}

exports.sendChatHelpMessageToAdmin = async (req, res) => {
    try {
        const maybeSentChatHelpMessage = await chatService.sendChatMessageToAdmin(req);
        if (!maybeSentChatHelpMessage) {
            return res.status(500).send('Could not send chat help message to admin');
        }
        return res.status(201).send(maybeSentChatHelpMessage);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        })
    }
}

exports.markChatMessagesAsRead = async (req, res) => {
    try {
        const maybeReadChatMessages = await chatService.markChatMessagesAsRead(req);
        if (!maybeReadChatMessages) {
            return res.status(500).send('Could not mark chat messages as read');
        }
        return res.status(201).send(maybeReadChatMessages);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        })
    }
}

exports.getChatNotifications = async (req, res) => {
    try {
        const maybeChatNotifications = await chatService.getChatNotifications(req);
        if (!maybeChatNotifications) {
            return res.status(500).send('Could not get chat notifications');
        }
        return res.status(201).send(maybeChatNotifications);
    } catch (e) {
        return res.status(500).json({
            error: e.message
        })
    }
}
