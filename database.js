let mongoose = require('mongoose');

const database = {}
mongoose.set('useFindAndModify', false);

database.connect = async (req, res) => {
    try {
        await mongoose.connect('mongodb://localhost:27017/ressources_relationnelles', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
    } catch (err) {
        throw err
    }
}

database.disconnect = async (req, res) => {
    try {
        await mongoose.connection.close();
    } catch (err) {
        throw err
    }
}

module.exports = database
