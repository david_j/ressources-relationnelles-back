const express = require("express");
const app = express();
const port = 8080;
const bodyParser = require('body-parser');
const cors = require('cors');
const seed = require('./config/seed');
const fileUpload = require('express-fileupload');

app.use(express.static('uploads/public'))
app.use(fileUpload({
    createParentPath: true
}));

const userRoutes = require('./routes/users');
const resourceRoutes = require('./routes/resource');
const resourceMessageRoutes = require('./routes/resourceMessage');
const relationRoutes = require('./routes/relation');
const relationRequestRoutes = require('./routes/relationRequest');
const authenticationRoutes = require('./routes/authentication');
const chatRoutes = require('./routes/chat');
const adminRoutes = require('./routes/admin');

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({limit: '500kb'}))

app.use('/api/', [userRoutes, resourceRoutes, resourceMessageRoutes, relationRoutes, relationRequestRoutes, authenticationRoutes, chatRoutes, adminRoutes]);

app.get("/", (req, res) => {
    res.send("<p>Bienvenue sur Ressources Relationnelles</p>");
});

let server = app.listen(port, async () => {
    // console.log("Server listening on port " + port);
    await seed.seedDatabase();
});

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/ressources_relationnelles", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
mongoose.set('useCreateIndex', true);


// SOCKET.IO //
const socket = require('socket.io')(server, {
    cors: {
        origins: ["http://localhost:4201", "https://res-rel.xyz"],
        methods: ["GET", "POST"]
    }
});

app.post('/send-chat-notification', (req, res) => {
    const notificationMessage = req.body;
    socket.emit('chat_message__' + notificationMessage.toUser, notificationMessage);
    res.status(200).send('Successfully received message');
})

socket.on('connection', socket => {
    // console.log('Socket: client connected');
    socket.on('chat_message', (event) => {
        const notificationMessage = event;
        socket.emit('chat_message__' + notificationMessage.toUser._id, notificationMessage);
    })
});
