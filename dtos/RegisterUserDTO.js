class RegisterUserDTO {
    username;
    email;
    firstName;
    lastName;
    birthDate;
    address;
    city;
    postalCode;
    password;
    activationKey;

    constructor(registerUser) {
        this.username = registerUser.username;
        this.email = registerUser.email;
        this.firstName = registerUser.firstName;
        this.lastName = registerUser.lastName;
        this.birthDate = registerUser.birthDate;
        this.address = registerUser.address;
        this.city = registerUser.city;
        this.postalCode = registerUser.postalCode;
        this.password = registerUser.password;
        this.activationKey = registerUser.activationKey;
    }
}

module.exports = RegisterUserDTO;
