class UserDTO {
    _id;
    username;
    email;
    firstName;
    lastName;
    birthDate;
    phone;
    genre;
    address;
    city;
    postalCode;
    profilePicture;
    role;
    isBanned;
    createdAt;
    editedAt;
    deletedAt;
    isEdited;
    isDeleted;

    constructor(user) {
        this._id = user._id;
        this.username = user.username;
        this.email = user.email;
        this.firstName = user.firstName;
        this.lastName = user.lastName;
        this.birthDate = user.birthDate;
        this.phone = user.phone;
        this.genre = user.genre;
        this.address = user.address;
        this.city = user.city;
        this.postalCode = user.postalCode;
        this.profilePicture = user.profilePicture;
        this.role = user.role;
        this.isBanned = user.isBanned;
        this.createdAt = user.createdAt;
        this.editedAt = user.editedAt;
        this.deletedAt = user.deletedAt;
        this.isEdited = user.isEdited;
        this.isDeleted = user.isDeleted;
    }
}

module.exports = UserDTO;
