class SessionDTO {
    id;
    username;
    email;
    role;

    constructor(sessionDTO) {
        this._id = sessionDTO._id;
        this.username = sessionDTO.username;
        this.email = sessionDTO.email;
        this.role = sessionDTO.role;
    }
}

module.exports = SessionDTO;
