class CredentialsDTO {
    email;
    password;

    constructor(credentialsDTO) {
        this.email = credentialsDTO.email;
        this.password = credentialsDTO.password;
    }
}

module.exports = CredentialsDTO;
