let Credentials = require('../dtos/CredentialsDTO');
let Session = require('../dtos/SessionDTO');
let RegisterUser = require('../dtos/RegisterUserDTO');
let User = require('../models/User');
let UserSettings = require('../models/UserSettings');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const mailService = require('../services/mailService');
const MailDTO = require('../dtos/MailDTO');
const randomString = require('@supercharge/strings')

exports.register = async (req) => {
    try {
        let registerUser = new RegisterUser(req.body);
        let newUser = new User(registerUser);
        newUser.password = await bcrypt.hash(newUser.password, 10);
        newUser.activationKey = await randomString.random(30);
        // REMOVE LATER
        newUser.isActivated = true;

        const maybeExistingUser = await User.findOne().or([
                {username: newUser.username},
                {email: newUser.email}
            ]
        );

        if (maybeExistingUser) {
            throw new Error('A user already exists with this username or email');
        }

        // return;
        const createdUser = await User.create(newUser)
            .then((doc) => {
                return doc;
            })
            .catch(err => {
                throw err;
            });

        const createdUserSettings = await UserSettings.create(
            {user: createdUser._id}
        )
            .then((doc) => {
                return doc;
            })
            .catch(err => {
                throw err;
            });

        await User.findOneAndUpdate(
            {_id: createdUser._id},
            {userSettings: createdUserSettings._id}
        )
            .then(doc => {
            })
            .catch(e => {
                throw e;
            })

        const activationLink = `http://localhost:4201/activation/${createdUser.activationKey}`;
        const activationMail = new MailDTO(
            null, createdUser.email, 'Activation de votre compte Relations Relationnelles', '',
            `
            <h2>Activation de votre compte</h2>
            <p>Bonjour,
            Afin d'activer votre compte Relations Relationnelles™, veuillez cliquer sur le lien ci-dessous :
            <a href="${activationLink}">Activer mon compte</a>
            </p>
            <hr>
            <p><b>À bientôt sur Ressources Relationnelles !</b></p>
            `, null
        );
        const maybeActivationEmail = await mailService.sendMail(activationMail, true);

        if (!maybeActivationEmail) {
            throw new Error('Could not send activation email');
        }

        return createdUser;

    } catch (e) {
        throw e;
    }
}

exports.activate = async (req) => {
    try {
        const activationKey = req.body.activationKey;
        if (!activationKey || activationKey === '' || activationKey === undefined) {
            throw new Error('Activation key is invalid');
        }

        const maybeUserByActivationKey = await User.findOne(
            {activationKey: activationKey},
            {},
            {},
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            }
        )

        if (!maybeUserByActivationKey) {
            throw new Error('Could not find user with this activation key');
        }

        if (maybeUserByActivationKey.isActivated) {
            throw new Error('User is already activated');
        }
        if (maybeUserByActivationKey.isBanned) {
            throw new Error('Cannot activate a banned user');
        }

        return await User.findOneAndUpdate(
            {_id: maybeUserByActivationKey._id},
            {isActivated: true, activationKey: ''},
            {strict: false},
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc.toDTO();
            }
        );

    } catch (e) {
        throw e;
    }
}

exports.login = async (req) => {
    try {
        const credentials = new Credentials(req.body);

        const maybeUser = await User.findOne(
            {email: credentials.email}
        ).exec();

        if (!maybeUser) {
            throw new Error('Email does not exist');
        }

        const match = await bcrypt.compare(credentials.password, maybeUser.password);

        if (!match) {
            throw new Error('Incorrect password');
        }

        const session = new Session(maybeUser.toDTO());
        const token = jwt.sign(JSON.stringify(session), 'shhhhh');

        if (!maybeUser.isActivated) {
            throw new Error('User is not activated');
        } else if (maybeUser.isDeleted) {
            throw new Error('User is banned');
        } else if (maybeUser.isBanned) {
            throw new Error('User was deleted');
        } else {
            return token;
        }
    } catch (e) {
        throw e;
    }
}
