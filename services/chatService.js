const ChatMessage = require('../models/ChatMessage');
const userService = require('./userService');

exports.findAllByCurrentUser = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        return await ChatMessage.find({isFromHelpChat: false})
            .or([
                {fromUser: currentUser._id},
                {toUser: currentUser._id}
            ])
            .populate('fromUser')
            .populate('toUser')
            .populate('responseToMessage')
            .then(docs => {
                return docs
            })
            .catch(err => {
                throw err;
            });
    } catch (e) {
        throw e;
    }
}

exports.findAllHelpMessagesByCurrentUser = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        return await ChatMessage.find().or([
                {fromUser: currentUser._id, isFromHelpChat: true},
                {toUser: currentUser._id, isFromHelpChat: true},
            ]
        )
            .populate('fromUser')
            .populate('toUser')
            .populate('responseToMessage')
            .then(docs => {
                return docs;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.sendChatMessage = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const chatMessage = req.body;
        chatMessage.fromUser = currentUser._id;

        return await ChatMessage.create(chatMessage)
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.sendChatMessageToAdmin = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const chatMessage = req.body;
        chatMessage.fromUser = currentUser._id;
        chatMessage.isFromHelpChat = true;

        return await ChatMessage.create(chatMessage)
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.markChatMessagesAsRead = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const maybeChatMessages = req.body;
        const maybeChatMessagesIdList = maybeChatMessages.map(message => message._id);

        const maybeMarkedAsReadMessages = await ChatMessage.updateMany(
            {_id: {$in: maybeChatMessagesIdList}, toUser: currentUser._id, isRead: false},
            {isRead: true}
        )
            .populate('fromUser')
            .populate('toUser')
            .populate('responseToMessage')
            .then(docs => {
                return docs;
            })
            .catch(err => {
                throw err;
            })

        return maybeMarkedAsReadMessages;
    } catch (e) {
        throw e;
    }
}

exports.getChatNotifications = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const unreadChatMessages = await ChatMessage.find(
            {toUser: currentUser._id, isRead: false}
        )
            .populate('fromUser')
            .populate('toUser')
            .populate('responseToMessage')
            .then(docs => {
                return docs;
            })
            .catch(err => {
                throw err;
            })

        const chatNotifications = {
            number: unreadChatMessages.length,
            docs: unreadChatMessages
        }
        return chatNotifications;
    } catch (e) {
        throw e;
    }
}
