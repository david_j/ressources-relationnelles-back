let RelationRequest = require('../models/RelationRequest');
const requestStatusEnum = require('../enums/RequestStatus.enum');
const userService = require('../services/userService');

exports.findAllByCurrentUser = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }
        return await RelationRequest.find().or([
            {
                fromUser: currentUser._id,
                status: requestStatusEnum.RequestStatusEnumNamesFromEnum.PENDING,
                isCancelled: false,
                isDeleted: false
            },
            {
                toUser: currentUser._id,
                status: requestStatusEnum.RequestStatusEnumNamesFromEnum.PENDING,
                isCancelled: false,
                isDeleted: false
            }
        ])
            .populate('fromUser')
            .populate('toUser')
            .then(docs => {
                return docs;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.findRelationRequestsByStatusByCurrentUser = async (req, status) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }
        return await RelationRequest.find({toUser: currentUser._id, status: status, isCancelled: false})
            .populate('toUser')
            .populate('fromUser')
            .then(docs => {
                return docs;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.findByCurrentUser = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }
        return await RelationRequest.find({user: currentUser._id, isCancelled: false})
            .populate('user')
            .populate('relationUser')
            .then(docs => {
                return docs;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.sendRelationRequest = async (req, userId) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        let newRelationRequest = new RelationRequest();
        newRelationRequest.fromUser = currentUser._id;
        newRelationRequest.toUser = userId;

        return await RelationRequest.create(newRelationRequest)
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.answerRelationRequest = async (req, relationRequestId, answer) => {
    try {
        const maybeExistingRelationRequest = await RelationRequest.findById(relationRequestId);
        if (!maybeExistingRelationRequest) {
            throw new Error('Could not find relation request to answer');
        }

        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find relation request user');
        }
        if (!maybeExistingRelationRequest.toUser._id.equals(currentUser._id)) {
            throw new Error('The relation request does not belong to this user');
        }
        if (maybeExistingRelationRequest.isAnswered) {
            throw new Error('The relation request was already answered');
        }

        return await RelationRequest.findOneAndUpdate(
            {_id: relationRequestId},
            {status: answer === 'accept' ? 'ACCEPTED' : 'REFUSED', isAnswered: true},
            null,
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            }
        );
    } catch
        (e) {
        throw e;
    }
}

exports.cancelRequest = async (req, relationRequestId) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }
        const maybeRequest = await RelationRequest.findById(relationRequestId);
        if (!maybeRequest) {
            throw new Error('Could not find relation request');
        }

        if (!maybeRequest.fromUser.equals(currentUser._id)) {
            throw new Error('Relation request does not belong to current user');
        }

        return await RelationRequest.findOneAndUpdate(
            {_id: relationRequestId},
            {isCancelled: true},
            {},
            ((err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            })
        );
    } catch (e) {
        throw e;
    }
}

exports.checkNotifications = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find relation request user');
        }
        const maybeRelationRequests = await this.findRelationRequestsByStatusByCurrentUser(req, requestStatusEnum.RequestStatusEnumNamesFromEnum.PENDING);
        if (!maybeRelationRequests) {
            throw new Error('Could not find relation requests');
        }

        const notifications = {
            number: maybeRelationRequests.length,
            docs: maybeRelationRequests
        }
        return notifications;
    } catch (e) {
        throw e;
    }
}
