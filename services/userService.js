let User = require('../models/User');
let UserSettings = require('../models/UserSettings');
const jwt = require('jsonwebtoken');
const RolesNamesFromEnum = require("../enums/Roles.enum").RolesNamesFromEnum;
const RolesValuesFromEnum = require("../enums/Roles.enum").RolesValuesFromEnum;
const relationService = require('../services/relationService');

// GET CURRENT USER
exports.getCurrent = async (req) => {
    try {
        const token = req.headers.authorization;
        const decodedToken = jwt.verify(token, 'shhhhh');

        return await User.findById(decodedToken._id)
            .populate('user_settings')
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            });

    } catch (e) {
        throw e;
    }
}

// UPDATE CURRENT USER
exports.updateCurrent = async (req) => {
    try {
        const token = req.headers.authorization;
        const decodedToken = jwt.verify(token, 'shhhhh');
        const updatedUser = req.body;

        return await User.findOneAndUpdate({_id: decodedToken._id}, updatedUser, {},
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            }
        );
    } catch (e) {
        throw e;
    }
}

// FIND USER BY ID
exports.findById = async (id) => {
    try {
        const maybeUser = await User.findById(id);

        if (maybeUser) {
            return maybeUser;
        } else {
            return null;
        }
    } catch (e) {
        throw e;
    }
}

exports.findByTextSearch = async (req, textSearch) => {
    try {
        const textSearchTerms = textSearch.split(' ');

        const currentUser = await this.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }
        const currentUserRelationList = await relationService.findAllByCurrentUser(req);
        if (!currentUserRelationList) {
            throw new Error('Could find current user relations');
        }

        const currentUserRelationIdList = currentUserRelationList.map(rel => rel.relationUser._id);

        return await User.find(
            {
                _id: {
                    $ne: currentUser._id,
                    $nin: currentUserRelationIdList
                },
                role: RolesNamesFromEnum.USER
            }).or([
            {username: {$in: textSearchTerms}},
            {email: {$in: textSearchTerms}},
            {firstName: {$in: textSearchTerms}},
            {lastName: {$in: textSearchTerms}}
        ])
            .then(docs => {
                return docs;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.hasRole = (user, role) => {
    try {
        return RolesValuesFromEnum[user.role] >= RolesValuesFromEnum[role];
    } catch (e) {
        throw e;
    }
}

exports.findSettingsForCurrentUser = async (req) => {
    try {
        const currentUser = await this.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }
        return await UserSettings.findOne({user: currentUser._id}, {}, {},
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            })
    } catch (e) {
        throw e;
    }
}

exports.updateSettingsForCurrentUser = async (req) => {
    try {
        const currentUser = await this.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }
        let updatedSettings = req.body;
        updatedSettings.user = currentUser._id;

        return await UserSettings.findOneAndUpdate({user: currentUser._id}, updatedSettings, {new: true, upsert: true},
            (err, docs) => {
                if (err) {
                    throw err;
                }
                return docs;
            })
    } catch (e) {
        throw e;
    }
}
