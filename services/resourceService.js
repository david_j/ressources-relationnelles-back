let Resource = require('../models/Resource');
let ResourceMessage = require('../models/ResourceMessage');
let Relation = require('../models/Relation');
let Event = require('../models/Event');
let EventResponse = require('../models/EventResponse');
const EventResponsesNamesFromAnswer = require("../enums/EventResponse.enum").EventResponsesNamesFromAnswer;
const userService = require('../services/userService');
const relationService = require('../services/relationService');
const md5 = require('md5');

// FIND ALL RESOURCES BY CURRENT USER
exports.findAllByCurrentUser = async (req) => {
    try {
        let currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        return await Resource.find({user: currentUser._id})
            .sort('-createdAt')
            .populate('user')
            .populate({
                path: 'messages',
                options: {
                    limit: 5,
                    sort: '-createdAt'
                },
                populate: {path: 'user'},
            })
            .then(res => {
                return res;
            })
            .catch(e => {
                throw e;
            });

    } catch (e) {
        throw e;
    }
}

// FIND ALL RESOURCES BY CURRENT USER
exports.findAllByCurrentUserRelations = async (req) => {
    try {
        let currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const currentUserRelationList = await relationService.findAllByCurrentUser(req);
        const currentUserRelationIdList = currentUserRelationList.map(
            rel => rel.relationUser._id
        );

        // return await Resource.find({user: {$ne: currentUser._id, $in: currentUserRelationIdList}, isDeleted: false})
        return await Resource.find({isDeleted: false}).or([
            {user: currentUser._id},
            {user: {$in: currentUserRelationIdList}},
        ])
            .populate([
                {
                    path: 'user'
                },
                {
                    path: 'event',
                    populate: ['user', {
                        path: 'responses',
                        populate: 'user'
                    }],
                },
                {
                    path: 'likes'
                },
                {
                    path: 'messages',
                    options: {
                        limit: 5,
                        sort: 'createdAt'
                    },
                    populate: 'user',
                }
            ])
            .sort('-createdAt')
            .then(res => {
                return res;
            })
            .catch(e => {
                throw e;
            });

    } catch (e) {
        throw e;
    }
}

// FIND RESOURCES FEED BY CURRENT USER
exports.findResourcesFeedByCurrentUser = async (req) => {
    try {
        let currentUser = await userService.getCurrent(req);

        const currentUserRelationList = await Relation.find({userId: currentUser._id});
        if (!currentUserRelationList) {
            return [];
        }

        const currentUserRelationListIds = currentUserRelationList.map(
            rel => rel.user._id
        );

        const maybeResourceList = await Resource.find()
            .where('userId').in(currentUserRelationListIds)
            .or([
                    {userId: currentUser._id}
                ]
            );
        if (maybeResourceList) {
            return maybeResourceList;
        } else {
            return [];
        }

    } catch (e) {
        throw e;
    }
}

exports.create = async (req, resource, image) => {
    try {
        let currentUser = await userService.getCurrent(req)

        let newResource = new Resource(resource);
        newResource.user = currentUser._id;

        let maybeUploadedImage;
        if (image) {
            const extension = image.name.substring(image.name.indexOf('.'));
            const encryptedImageName = md5(image.name) + extension;
            maybeUploadedImage = image.mv('./uploads/public/images/resources/' + newResource._id + '/' + encryptedImageName);
            newResource.image = encryptedImageName;
        }

        if (newResource.type === 'event') {
            const newEvent = new Event;
            newEvent.user = currentUser;
            newEvent.title = newResource.title;
            newEvent.message = newResource.message;
            newEvent.startDate = newResource.eventStartDate;
            newEvent.endDate = newResource.eventEndDate;
            const maybeCreatedEvent = await Event.create(newEvent)
                .then(doc => {
                    newResource.event = doc;
                    return doc;
                })
                .catch(err => {
                    throw err;
                })
        }

        const maybeCreatedResource = await Resource.create(newResource)
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            });

        return maybeCreatedResource;
    } catch (e) {
        throw e;
    }
}

exports.sendMessage = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }
        const message = req.body;
        const resourceId = message.resource;
        let newMessage = new ResourceMessage(req.body);
        const maybeExistingResource = await Resource.findById(resourceId)
            .populate('messages')
            .catch(err => {
                throw err;
            });
        if (!maybeExistingResource) {
            throw new Error('Could not find existing resource');
        }
        newMessage.user = currentUser.id;
        newMessage.resource = resourceId;

        return await ResourceMessage.create(newMessage)
            .then(message => {
                return Resource.findOneAndUpdate({_id: message.resource},
                    {$push: {messages: message._id}}
                )
                    .then(resource => {
                        return resource;
                    })
                    .catch(err => {
                        throw err;
                    })
            })
            .catch(err => {
                throw err;
            });
    } catch (e) {
        console.error(e);
        throw e;
    }
}

exports.updateResource = async (req, resource, image) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const resourceToUpdate = resource;

        let maybeUploadedImage;
        if (image) {
            const extension = image.name.substring(image.name.indexOf('.'));
            const encryptedImageName = md5(image.name) + extension;
            maybeUploadedImage = image.mv('./uploads/public/images/resources/' + resourceToUpdate._id + '/' + encryptedImageName);
            resourceToUpdate.image = encryptedImageName;
        }

        resourceToUpdate.isEdited = true;
        resourceToUpdate.editedAt = Date.now();

        // if (!resourceToUpdate.user._id.equals(currentUser._id)) {
        //     throw new Error('Not allowed to update user');
        // }

        return await Resource.findOneAndUpdate(
            {_id: resourceToUpdate._id},
            resourceToUpdate,
            {strict: false}
        )
            .then(doc => {
                return doc;
            })
            .catch(err => {
                console.error(err);
                throw err;
            })
    } catch (e) {
        console.error(e);
        throw e;
    }
}

exports.deleteResource = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }
        const maybeResourceToDeleteId = req.params.id;

        return Resource.findOneAndUpdate({_id: maybeResourceToDeleteId}, {
            deletedAt: Date.now(),
            isDeleted: true
        }, {strict: false})
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.uploadResourceImage = async (req, image) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }
        const uploadedImage = await image.mv('./uploads/public/images/resources/' + currentUser._id + '/' + image.name);
        if (!uploadedImage) {
            throw new Error('Could not upload image');
        }

        return uploadedImage;
    } catch (e) {
        throw e;
    }
}

exports.answerEvent = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const eventId = req.params.id;
        const eventAnswer = req.params.answer;
        const eventResponse = new EventResponse();
        eventResponse.event = eventId;
        eventResponse.user = currentUser;
        eventResponse.response = EventResponsesNamesFromAnswer[eventAnswer.toUpperCase()];

        const createdEventResponse = await EventResponse.create(eventResponse)
            .then(doc => {
                // console.log(doc);
                return doc;
            })
            .catch(err => {
                // console.log(err);
                throw err;
            })

        return await Event.findOneAndUpdate(
            {_id: eventId},
            {$push: {responses: createdEventResponse._id}}
        )
            .then(doc => {
                // console.log(doc);
                return doc;
            })
            .catch(err => {
                throw err;
            })

    } catch (e) {
        throw e;
    }
}

exports.likeResource = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const resourceToLikeId = req.params.id;

        return await Resource.findOneAndUpdate(
            {_id: resourceToLikeId},
            {$push: {likes: currentUser._id}}
        )
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.unlikeResource = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const resourceToLikeId = req.params.id;

        return await Resource.findOneAndUpdate(
            {_id: resourceToLikeId},
            {$pull: {likes: currentUser._id}}
        )
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}
