const adminUserService = require('../../services/admin/adminUserService');
const adminResourceService = require('../../services/admin/adminResourceService');
const adminResourceMessageService = require('../../services/admin/adminResourceMessageService');
const adminRelationService = require('../../services/admin/adminRelationService');

exports.getDashboardStats = async (req) => {
    const users = await adminUserService.findAll(req);
    const resources = await adminResourceService.findAll(req);
    const resourceMessages = await adminResourceMessageService.findAll(req);
    const relations = await adminRelationService.findAll(req);

    return [users, resources, resourceMessages, relations];
}
