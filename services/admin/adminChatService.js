const ChatMessage = require('../../models/ChatMessage');
const userService = require('../userService');

exports.findAll = async () => {
    try {
        return ChatMessage.find()
            .populate('fromUser')
            .populate('toUser')
            .populate('responseToMessage')
            .then(docs => {
                return docs;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.sendChatMessageToUser = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const chatMessage = req.body;
        chatMessage.fromUser = currentUser._id;
        chatMessage.isFromHelpChat = true;

        return await ChatMessage.create(chatMessage)
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}
