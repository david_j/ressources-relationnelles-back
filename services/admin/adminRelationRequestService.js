let database = require('../../database');
let RelationRequest = require('../../models/RelationRequest');
const userService = require('../../services/userService');

exports.findAll = async (req) => {
    return await RelationRequest.find()
        .populate(['fromUser', 'toUser'])
        .then(docs => {
            return docs;
        })
        .catch(e => {
            throw e;
        })
}

exports.findById = async (req) => {

}

exports.create = async (req) => {

}

exports.update = async (req) => {

}

exports.delete = async (req) => {

}
