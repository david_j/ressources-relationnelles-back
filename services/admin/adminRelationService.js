let Relation = require('../../models/Relation');
const relationService = require('../../services/relationService');
const userService = require('../../services/userService');
const RolesValuesFromEnum = require("../../enums/Roles.enum").RolesValuesFromEnum;

exports.findAll = async (req) => {
    return await Relation.find()
        .populate('user')
        .populate('relationUser')
        .then(docs => {
            return docs
        })
        .catch(err => {
            throw err;
        })
}

exports.findRelationById = async (id) => {
    try {
        return await Relation.findById(id)
            .populate('user')
            .then(res => {
                return res;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.create = async (req) => {
    try {
        let newRelation = new Relation(req.body);

        return await Relation.create(newRelation)
            .then(res => {
                return res;
            })
            .catch(e => {
                throw e;
            });
    } catch (e) {
        throw e;
    }
}

exports.update = async (req) => {
    try {
        const relationId = req.body._id;
        const maybeRelation = await Relation.findById(relationId);

        if (!maybeRelation) {
            throw new Error('Cannot find relation to update');
        }

        let updatedRelation = req.body;
        updatedRelation.isEdited = true;
        updatedRelation.editedAt = Date.now();

        return await Relation.findOneAndUpdate(
            {_id: maybeRelation._id},
            updatedRelation,
            {strict: false},
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            })
    } catch (e) {
        throw e;
    }
}

exports.delete = async (req) => {
    try {
        const relationId = req.params.id;
        const maybeRelation = await this.findRelationById(relationId);
        if (!maybeRelation) {
            throw new Error('Could not find relation to delete');
        }

        if (!(await this.allowedToUpdateRelation(req, maybeRelation))) {
            throw new Error('Not allowed to delete this relation');
        }

        return await Relation.findOneAndUpdate(
            {_id: relationId},
            {isDeleted: true, deletedAt: Date.now()},
            {strict: false},
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            })
    } catch (e) {
        throw e;
    }
}

//TF that means ?
exports.ban = async (req) => {
    try {
        const relationId = req.params.id;
        if (!relationId) {
            throw new Error('No relation id specified');
        }

        let currentUser = await userService.getCurrent(req);
        const maybeRelationToBan = await this.findRelationById(relationId);

        if (!maybeRelationToBan) {
            throw new Error(`Could not find relation with id ${relationId}`);
        } else {
            if (RolesValuesFromEnum[maybeRelationToBan.user.role] >= RolesValuesFromEnum[currentUser.role]) {
                throw new Error('Not allowed to ban this relation');
            }
        }

        const banRelation = (req.params.ban === 'true');

        const savedRelationToBan = await Relation.findOneAndUpdate(
            {_id: relationId},
            {isBanned: banRelation},
            {strict: false},
            (err) => {
                if (err) {
                    throw err;
                }
            }
        );

        if (savedRelationToBan) {
            if (savedRelationToBan.isBanned === banRelation) {
                return savedRelationToBan;
            } else {
                throw new Error('Could not update relation ban property');
            }
        } else {
            throw new Error(`An error occurred while trying to update relation ${relationId}`)
        }
    } catch (e) {
        throw e;
    }
}

exports.allowedToUpdateRelation = async (req, relationToUpdate) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        return RolesValuesFromEnum[currentUser.role] > RolesValuesFromEnum[relationToUpdate.user.role];
    } catch (e) {
        throw e;
    }
}
