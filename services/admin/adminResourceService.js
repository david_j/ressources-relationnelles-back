let Resource = require('../../models/Resource');
const resourceService = require('../../services/resourceService');
const userService = require('../../services/userService');
const RolesValuesFromEnum = require("../../enums/Roles.enum").RolesValuesFromEnum;

exports.findAll = async (req) => {
    try {
        return await Resource.find()
            .then(docs => {
                return docs;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}


exports.findResourceById = async (id) => {
    try {
        return await Resource.findById(id)
            .populate('user')
            .then(res => {
                return res;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.create = async (req) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        let newResource = new Resource(req.body);
        newResource.user = currentUser._id;

        return newResource.save()
            .then((res) => {
                return res;
            })
            .catch(err => {
                throw err;
            });
    } catch (e) {
        throw e;
    }
}

exports.update = async (req) => {
    try {
        const resourceId = req.body._id;
        const maybeResource = await Resource.findById(resourceId);

        if (!maybeResource) {
            throw new Error('Cannot find resource to update');
        }

        let updatedResource = req.body;
        updatedResource.isEdited = true;
        updatedResource.editedAt = Date.now();

        return await Resource.findOneAndUpdate(
            {_id: maybeResource._id},
            updatedResource,
            {strict: false},
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            })
    } catch (e) {
        throw e;
    }
}

exports.delete = async (req) => {
    try {
        const resourceId = req.params.id;
        const maybeResource = await this.findResourceById(resourceId);
        if (!maybeResource) {
            throw new Error('Could not find resource to delete');
        }

        if (!(await this.allowedToUpdateResource(req, maybeResource))) {
            throw new Error('Not allowed to delete this resource');
        }

        return await Resource.findOneAndUpdate(
            {_id: resourceId},
            {isDeleted: true, deletedAt: Date.now()},
            {strict: false},
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            })
    } catch (e) {
        throw e;
    }
}

exports.ban = async (req) => {
    try {
        const resourceId = req.params.id;
        if (!resourceId) {
            throw new Error('No resource id specified');
        }

        let currentUser = await userService.getCurrent(req);
        const maybeResourceToBan = await this.findResourceById(resourceId);

        if (!maybeResourceToBan) {
            throw new Error(`Could not find resource with id ${resourceId}`);
        } else {
            if (RolesValuesFromEnum[maybeResourceToBan.user.role] >= RolesValuesFromEnum[currentUser.role]) {
                throw new Error('Not allowed to ban this resource');
            }
        }

        const banResource = (req.params.ban === 'true');

        const savedResourceToBan = await Resource.findOneAndUpdate(
            {_id: resourceId},
            {isBanned: banResource},
            {strict: false},
            (err) => {
                if (err) {
                    throw err;
                }
            }
        );

        if (savedResourceToBan) {
            if (savedResourceToBan.isBanned === banResource) {
                return savedResourceToBan;
            } else {
                throw new Error('Could not update resource ban property');
            }
        } else {
            throw new Error(`An error occurred while trying to update resource ${resourceId}`)
        }
    } catch (e) {
        throw e;
    }
}
exports.allowedToUpdateResource = async (req, resourceToUpdate) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        return RolesValuesFromEnum[currentUser.role] > RolesValuesFromEnum[resourceToUpdate.user.role];
    } catch (e) {
        throw e;
    }
}
