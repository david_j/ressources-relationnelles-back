let Resource = require('../../models/Resource');
let ResourceMessage = require('../../models/ResourceMessage');
let Relation = require('../../models/Relation');
const userService = require('../../services/userService');

exports.findAll = async (req) => {
    try {
        return await ResourceMessage.find()
            .then(docs => {
                return docs;
            })
            .catch(err => {
                throw err;
            })
    } catch (e) {
        throw e;
    }
}

exports.findById = async (req) => {

}

exports.create = async (req) => {

}

exports.update = async (req) => {

}

exports.delete = async (req) => {

}
