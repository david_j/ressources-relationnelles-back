let User = require('../../models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const RolesValuesFromEnum = require("../../enums/Roles.enum").RolesValuesFromEnum;
const userService = require('../../services/userService');
const {RolesEnum} = require("../../enums/Roles.enum");

exports.findAll = async (req) => {
    try {
        return await User.find(
            (err, docs) => {
                if (err) {
                    throw err;
                }
                return docs;
            }
        );
    } catch (e) {
        throw e;
    }
}

exports.findById = async (id) => {
    try {
        return await User.findById(id, {}, {},
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            })
    } catch (e) {
        throw e;
    }
}

exports.create = async (req) => {
    try {
        const maybeExistingUser = await User.findOne().or([
            {username: req.body.username},
            {email: req.body.email}
        ])

        if (maybeExistingUser != null) {
            throw new Error('A user already exists with this username or email');
        }

        let newUser = new User(req.body);
        newUser.password = 'test';
        newUser.password = await bcrypt.hash(newUser.password, 10);

        return newUser.save()
            .then((res) => {
                return res;
            })
            .catch(err => {
                throw err;
            });
    } catch (e) {
        throw e;
    }
}

exports.update = async (req) => {
    try {
        const userId = req.body._id;
        const maybeUser = await User.findById(userId);

        if (maybeUser) {
            if (!(await this.allowedToUpdateUser(req, maybeUser))) {
                throw new Error('Not allowed to update this user');
            }
        } else {
            throw new Error('Cannot find user to update');
        }

        let updatedUser = req.body;

        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }
        if (RolesValuesFromEnum[updatedUser.role] > RolesValuesFromEnum.ADMIN) {
            throw new Error('Cannot update a user with a role higher than admin');
        }

        updatedUser.isEdited = true;
        updatedUser.editedAt = Date.now();

        return await User.findOneAndUpdate({_id: maybeUser._id}, updatedUser, {strict: false},
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            })
    } catch (e) {
        throw e;
    }
}

exports.delete = async (req) => {
    try {
        const userId = req.params.id;
        const maybeUser = await this.findById(userId);
        if (!maybeUser) {
            throw new Error('Could not find user to delete');
        }

        if (!(await this.allowedToUpdateUser(req, maybeUser))) {
            throw new Error('Not allowed to delete this user');
        }

        return await User.findOneAndUpdate(
            {_id: userId},
            {isDeleted: true, deletedAt: Date.now()},
            {strict: false},
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            })
    } catch (e) {
        throw e;
    }
}

exports.ban = async (req) => {
    try {
        const userId = req.params.id;
        if (!userId) {
            throw new Error('No user id specified');
        }

        let currentUser = await userService.getCurrent(req);
        const maybeUserToBan = await this.findById(userId);

        if (!maybeUserToBan) {
            throw new Error(`Could not find user with id ${userId}`);
        } else {
            if (RolesValuesFromEnum[maybeUserToBan.role] >= RolesValuesFromEnum[currentUser.role]) {
                throw new Error('Not allowed to ban this user');
            }
        }

        const banUser = (req.params.ban === 'true');

        const savedUserToBan = await User.findOneAndUpdate(
            {_id: userId},
            {isBanned: banUser},
            {strict: false},
            (err) => {
                if (err) {
                    throw err;
                }
            }
        );

        if (savedUserToBan) {
            if (savedUserToBan.isBanned === banUser) {
                return savedUserToBan;
            } else {
                throw new Error('Could not update user ban property');
            }
        } else {
            throw new Error(`An error occurred while trying to update user ${userId}`)
        }
    } catch (e) {
        throw e;
    }
}

exports.allowedToUpdateUser = async (req, userToUpdate) => {
    try {
        const currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        return RolesValuesFromEnum[currentUser.role] > RolesValuesFromEnum[userToUpdate.role];
    } catch (e) {
        throw e;
    }
}
