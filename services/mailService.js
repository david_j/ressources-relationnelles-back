const nodemailer = require("nodemailer");
const fs = require('fs');

exports.sendMail = async (mailDTO, fromResRel) => {
    try {
        const mailSettings = await fs.readFileSync('./config/mail-config.json', 'utf-8');
        if (!mailSettings) {
            throw new Error('Could not load mail settings');
        }
        if (fromResRel) {
            mailDTO.from = JSON.parse(mailSettings).auth.user;
        }
        // return;
        // let testAccount = await nodemailer.createTestAccount();

        // create reusable transporter object using the default SMTP transport
        const transporter = nodemailer.createTransport(
            JSON.parse(mailSettings)
        );

        // send mail with defined transport object
        let info = await transporter.sendMail(
            mailDTO
        );

        return info;
    } catch (e) {
        console.error(e);
        throw e;
    }
}
