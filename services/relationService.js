const mongoose = require('mongoose');
let Relation = require('../models/Relation');
let User = require('../models/User');
const userService = require('../services/userService');
const relationService = require('../services/relationService');
const relationRequestService = require('../services/relationRequestService');
const RolesEnum = require('../enums/Roles.enum')

// FIND ALL RELATIONS BY CURRENT USER
exports.findAllByCurrentUser = async (req) => {
    try {
        let currentUser = await userService.getCurrent(req);

        return Relation.find({
                user: {
                    _id: currentUser._id,
                    isBanned: false,
                    isDeleted: false
                }
            }
        )
            .populate('user')
            .populate('relationUser')
            .sort('-createdAt')
            .then(res => {
                return res.map(doc => {
                    if (doc.user) {
                        doc.user = doc.user.toDTO();
                    }
                    return doc;
                })
            })
            .catch(e => {
                throw e;
            });
    } catch (e) {
        throw e;
    }
}

// exports.createRelation = async (req, resource) => {
exports.create = async (req, res) => {
    try {
        let currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            return res.status(404).send('Could not find current user');
        }

        let newRelation = new Relation();

        newRelation.user = currentUser._id;
        newRelation.relationUser = req.params.relationUser

        return newRelation.save()
            .then(res => {
                return res;
            })
            .catch(e => {
                throw e;
            });
    } catch (e) {
        throw e;
    }
}

exports.createFromAcceptedRequest = async (req, acceptedRequest) => {
    try {
        let newCurrentUserRelation = new Relation();
        newCurrentUserRelation.user = acceptedRequest.fromUser;
        newCurrentUserRelation.relationUser = acceptedRequest.toUser;

        let newFromUserRelation = new Relation();
        newFromUserRelation.user = acceptedRequest.toUser;
        newFromUserRelation.relationUser = acceptedRequest.fromUser;

        const currentUserRelation = await Relation.create(newCurrentUserRelation)
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            })

        const fromUserRelation = await Relation.create(newFromUserRelation)
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            })

        if (currentUserRelation && fromUserRelation) {
            return [currentUserRelation, fromUserRelation];
        }
    } catch (e) {
        throw e;
    }
}

exports.update = async (req, res) => {
    try {
        let currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            return res.status(404).send('Could not find current user');
        }

        let updateRelation = await Relation.findById(
            req.params.id,
            {},
            {},
            (err, doc) => {
                if (err) {
                    throw err;
                }
            }
        );

        if (!updateRelation) {
            throw new Error('Could not find existing relation');
        }
        if (!updateRelation.user.equals(currentUser._id)) {
            throw new Error('The resource user id does not match current user id');
        }

        updateRelation.user = currentUser._id;
        updateRelation.relationUser = req.params.relationUser;

        return updateRelation.save()
            .then(res => {
                return res;
            })
            .catch(e => {
                throw e;
            });
    } catch (e) {
        throw e;
    }
}

exports.delete = async (req) => {
    try {
        const relationId = req.params.id;
        const relation = await this.findRelationById(relationId);
        if (!relation) {
            throw new Error('Could not find relation to delete');
        }

        if (!(await this.allowedToUpdateRelation(req, relation))) {
            throw new Error('Not allowed to delete this relation');
        }

        return await Relation.findOneAndUpdate(
            {_id: relationId},
            {isDeleted: true, deletedAt: Date.now()},
            {strict: false},
            (err, doc) => {
                if (err) {
                    throw err;
                }
                return doc;
            })
    } catch (e) {
        throw e;
    }
}

exports.findById = async (id) => {
    try {
        const relation = await Relation.findById(id);

        if (relation) {
            return relation;
        } else {
            return null;
        }
    } catch (e) {
        throw e;
    }
}

exports.findAll = async (req) => {
    try {
        let currentUser = await userService.getCurrent(req);

        return Relation.find({user: currentUser._id})
            .populate('user')
            .populate('relationUser')
            .sort('-createdAt')
            .then(res => {
                return res.map(doc => {
                    if (doc.user) {
                        doc.user = doc.user.toDTO();
                    }
                    return doc;
                })
            })
            .catch(e => {
                throw e;
            });
    } catch (e) {
        throw e;
    }
}

exports.getSuggestions = async (req) => {
    try {
        let currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const currentUserRelationList = await relationService.findAllByCurrentUser(req);
        if (!currentUserRelationList) {
            throw new Error('Could not find current user relation list');
        }

        const currentUserRelationIdList = currentUserRelationList.map(
            relation => new mongoose.Types.ObjectId(relation.relationUser._id)
        );

        const currentUserRelationRequestList = await relationRequestService.findAllByCurrentUser(req);
        if (!currentUserRelationList) {
            throw new Error('Could not find current user relation list');
        }

        const currentUserRelationRequestIdList = currentUserRelationRequestList.map(
            relation => new mongoose.Types.ObjectId(relation.toUser._id)
        );

        return User.find(
            {
                role: RolesEnum.RolesNamesFromEnum.USER,
                isBanned: false,
                isDeleted: false
            }
        ).and([
            {_id: {$ne: currentUser._id}},
            {_id: {$nin: currentUserRelationIdList}},
            {_id: {$nin: currentUserRelationRequestIdList}}
        ])
            .sort('-createdAt')
            .then(docs => {
                return docs;
            })
            .catch(e => {
                throw e;
            });
    } catch (e) {
        throw e;
    }
}

exports.createRelationsFromAcceptedRequest = async (req, acceptedRequest) => {
    try {
        let newCurrentUserRelation = new Relation();
        newCurrentUserRelation.user = acceptedRequest.fromUser;
        newCurrentUserRelation.relationUser = acceptedRequest.toUser;

        let newFromUserRelation = new Relation();
        newFromUserRelation.user = acceptedRequest.toUser;
        newFromUserRelation.relationUser = acceptedRequest.fromUser;

        const currentUserRelation = await Relation.create(newCurrentUserRelation)
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            })

        const fromUserRelation = await Relation.create(newFromUserRelation)
            .then(doc => {
                return doc;
            })
            .catch(err => {
                throw err;
            })

        if (currentUserRelation && fromUserRelation) {
            return [currentUserRelation, fromUserRelation];
        }
    } catch (e) {
        throw e;
    }
}

exports.deleteRelationsByRelationUser = async (req, relationUserId) => {
    try {
        let currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const relationsToDelete = await Relation.find({isDeleted: false}).or([
            {user: currentUser._id, relationUser: relationUserId},
            {user: relationUserId, relationUser: currentUser._id}
        ])
        const relationsToDeleteIdList = relationsToDelete.map(
            rel => rel._id
        )

        const maybeDeletedRelations = await Relation.updateMany(
            {_id: {$in: relationsToDeleteIdList}},
            {isDeleted: true, deletedAt: Date.now()},
            {},
            (err, docs) => {
                if (err) {
                    throw err;
                }
            }
        );

        return maybeDeletedRelations;
    } catch (e) {
        throw e;
    }
}

exports.isInCurrentUserRelationList = async (req) => {
    try {
        let currentUser = await userService.getCurrent(req);
        if (!currentUser) {
            throw new Error('Could not find current user');
        }

        const userId = req.params.id;

        return await Relation.findOne(
            {user: currentUser._id, relationUser: userId, isDeleted: false}
        )
            .populate({
                path: 'relationUser',
                populate: {
                    path: 'userSettings'
                }
            })
            .then(doc => {
                if (doc.relationUser.userSettings.publicProfile) {
                    return doc;
                } else {
                    throw new Error('This profile is private');
                }
            })
            .catch(err => {
                throw err;
            })

    } catch (e) {
        throw e;
    }
}
