const express = require('express');
const router = express.Router();
const resourceController = require('../controllers/resourceController');
const auth = require('../middleware/auth');

// ROUTES WITHOUT PARAMETERS
router.get('/resources/by-current-user', auth, resourceController.findAllByCurrentUser);    // FIND ALL RESOURCES BY CURRENT USER
router.get('/resources/by-current-user-relations', auth, resourceController.findAllByCurrentUserRelations);    // FIND ALL RESOURCES BY CURRENT USER RELATIONS
router.post('/resources', auth, resourceController.create);                                 // CREATE A RESOURCE
router.put('/resources', auth, resourceController.update);                                  // UPDATE A RESOURCE
router.delete('/resources/:id', auth, resourceController.delete);                   // DELETE A RESOURCE

router.get('/resources/like/:id', auth, resourceController.likeResource);                   // LIKE A RESOURCE
router.get('/resources/unlike/:id', auth, resourceController.unlikeResource);                   // UNLIKE A RESOURCE

router.get('/resources/events/answer/:id/:answer', auth, resourceController.answerEvent);                   // ANSWER A RESOURCE EVENT

router.post('/resources/upload-image', auth, resourceController.uploadResourceImage); // UPLOAD A RESOURCE IMAGE

router.get('/resources/feed', auth, resourceController.findRelationsFeedByCurrentUser);     // FIND RESOURCES FEED BY CURRENT USER

// ROUTES INCLUDING PARAMETERS
router.get('/resources/:id', auth, resourceController.findResourceById); // FIND RESOURCE BY ID


module.exports = router;
