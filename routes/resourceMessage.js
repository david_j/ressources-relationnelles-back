const express = require('express');
const router = express.Router();
const resourceController = require('../controllers/resourceController');
const auth = require('../middleware/auth');

// ROUTES WITHOUT PARAMETERS

router.get('/resources/messages/by-current-user', auth, resourceController.findAllByCurrentUser);   // FIND RESOURCE MESSAGES
router.post('/resources/messages', auth, resourceController.create);                                // CREATE A RESOURCE MESSAGE
// router.put('/resources/messages', auth, resourceController.update);                                 // EDIT A RESOURCE MESSAGE
// router.delete('/resources/messages/:id', auth, resourceController.delete);                    // DELETE A RESOURCE MESSAGE

router.post('/resources/messages/send', auth, resourceController.sendResourceMessage); // SEND A RESOURCE MESSAGE

// ROUTES INCLUDING PARAMETERS
// router.get('/resources/messages/:id', auth, resourceController.findById);     // FIND RESOURCE MESSAGE BY ID


module.exports = router;
