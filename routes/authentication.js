const express = require('express');
const router = express.Router();
const authenticationController = require('../controllers/authenticationController')
const auth = require('../middleware/auth');


router.post('/login', authenticationController.login);
router.post('/register', authenticationController.register);
router.post('/activate', authenticationController.activate);
router.post('/verify-token', authenticationController.verifyToken);
router.get('/authentication/is-user', auth, authenticationController.isUser);
router.get('/authentication/is-admin', auth, authenticationController.isAdmin);
router.get('/authentication/is-super-admin', auth, authenticationController.isSuperAdmin);

module.exports = router;
