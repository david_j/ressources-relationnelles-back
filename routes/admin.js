const express = require('express');
const router = express.Router();
const adminStatsController = require('../controllers/admin/adminStatsController');
const adminUserController = require('../controllers/admin/adminUserController');
const adminResourceController = require('../controllers/admin/adminResourceController');
const adminResourceMessageController = require('../controllers/admin/adminResourceMessageController');
const adminRelationController = require('../controllers/admin/adminRelationController');
const adminRelationRequestController = require('../controllers/admin/adminRelationRequestController');
const adminChatController = require('../controllers/admin/adminChatController');

const chatController = require('../controllers/chatController');
const authAdmin = require('../middleware/auth-admin');

// GENERAL
router.get('/admin/dashboard/stats', authAdmin, adminStatsController.getDashboardStats)

// USERS
router.get('/admin/users', authAdmin, adminUserController.findAll);          // FIND ALL USERS
router.post('/admin/users', authAdmin, adminUserController.create);          // CREATE A USER
router.put('/admin/users', authAdmin, adminUserController.update);           // UPDATE A USER
router.delete('/admin/users/:id', authAdmin, adminUserController.delete);    // DELETE A USER
router.get('/admin/users/ban/:id/:ban', authAdmin, adminUserController.ban); // BAN A USER

router.get('/admin/users/:id', authAdmin, adminUserController.findById); // FIND USER BY ID

// RESOURCES
router.get('/admin/resources', authAdmin, adminResourceController.findAll);         // FIND ALL RESOURCES
router.post('/admin/resources', authAdmin, adminResourceController.create);         // CREATE A RESOURCE
router.put('/admin/resources', authAdmin, adminResourceController.update);          // UPDATE A RESOURCE
router.delete('/admin/resources/:id', authAdmin, adminResourceController.delete);   // DELETE A RESOURCE
router.get('/admin/resources/ban/:id/:ban', authAdmin, adminResourceController.ban); // BAN A RESOURCE

// router.get('/admin/resources/:id', authAdmin, adminResourceController.findById);    // FIND RESOURCE BY ID

// RESOURCE MESSAGES
router.get('/admin/resources/messages', authAdmin, adminResourceMessageController.findAll);          // FIND RESOURCE MESSAGES
// router.post('/admin/resources/messages', authAdmin, adminResourceMessageController.create);          // CREATE A RESOURCE MESSAGE
// router.put('/admin/resources/messages', authAdmin, adminResourceMessageController.update);           // EDIT A RESOURCE MESSAGE
// router.delete('/admin/resources/messages/:id', authAdmin, adminResourceMessageController.delete);    // DELETE A RESOURCE MESSAGE

// router.get('/admin/resources/message/:id', authAdmin, adminResourceMessageController.findById);      // FIND RESOURCE MESSAGE BY ID

// RELATIONS
router.get('/admin/relations', authAdmin, adminRelationController.findAll);         // FIND ALL RELATIONS
router.post('/admin/relations', authAdmin, adminRelationController.create);         // CREATE A RELATION
// router.put('/admin/relations', authAdmin, adminRelationController.update);          // UPDATE A RELATION
router.delete('/admin/relations/:id', authAdmin, adminRelationController.delete);       // DELETE A RELATION

// router.get('/admin/relations/:id', authAdmin, adminRelationController.findById);    // FIND RELATION BY ID

// RELATION REQUESTS
router.get('/admin/relation-requests', authAdmin, adminRelationRequestController.findAll);          // FIND ALL RELATION REQUESTS
router.post('/admin/relation-requests', authAdmin, adminRelationRequestController.create);          // CREATE A RELATION REQUEST
// router.put('/admin/relation-requests', authAdmin, adminRelationRequestController.update);           // UPDATE A RELATION REQUEST
router.delete('/admin/relation-requests/:id', authAdmin, adminRelationRequestController.delete);    // DELETE A RELATION REQUEST

// CHAT
router.get('/admin/chat', authAdmin, adminChatController.findAll);
router.post('/admin/chat', authAdmin, adminChatController.sendChatMessageToUser);

module.exports = router;
