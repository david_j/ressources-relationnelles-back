const express = require('express');
const router = express.Router();
const relationRequestController = require('../controllers/relationRequestController');
const auth = require('../middleware/auth');

// ROUTES WITHOUT PARAMETERS
router.get('/relation-requests/by-current-user', auth, relationRequestController.findAllByCurrentUser);     // FIND ALL RELATION REQUESTS BY CURRENT USER
router.post('/relation-requests/send', auth, relationRequestController.sendRelationRequest);                // SEND RELATION REQUEST
router.get('/relation-requests/notifications', auth, relationRequestController.checkNotifications); // CHECK RELATION REQUESTS NOTIFICATIONS

// ROUTES INCLUDING PARAMETERS
router.get('/relation-requests/:id', auth, relationRequestController.findById);                             // FIND RELATION REQUEST BY ID
router.get('/relation-requests/answer/:id/:answer', auth, relationRequestController.answerRelationRequest); // ANSWER RELATION REQUEST
router.get('/relation-requests/cancel/:id', auth, relationRequestController.cancelRelationRequest); // CHECK RELATION REQUESTS NOTIFICATIONS


module.exports = router;
