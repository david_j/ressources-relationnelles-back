const express = require('express');
const router = express.Router();
const relationController = require('../controllers/relationController');
const userController = require('../controllers/userController');
const auth = require('../middleware/auth');

// ROUTES WITHOUT PARAMETERS
router.get('/relations/by-current-user', auth, relationController.findAllByCurrentUser);
router.get('/relations/get-suggestions', auth, relationController.getSuggestions);
router.get('/relation-requests/by-current-user', auth, relationController.findAllRelationRequestsByCurrentUser);                // GET USERS FOR RELATION SUGGESTION LIST
router.get('/relation-requests/:status/by-current-user', auth, relationController.findRelationRequestsByStatusByCurrentUser);                // GET USERS FOR RELATION SUGGESTION LIST
router.get('/relation-requests/send-request/:userId', auth, relationController.sendRelationRequest);                // GET USERS FOR RELATION SUGGESTION LIST

router.delete('/relation-requests/by-relation-user/:relationUserId', auth, relationController.deleteByRelationUser);                // GET USERS FOR RELATION SUGGESTION LIST

// ROUTES INCLUDING PARAMETERS //
router.get('/relations/:id', auth, relationController.findById);

router.get('/relations/is-relation-to-current-user/:id', auth, relationController.isInCurrentUserRelationList);

module.exports = router;
