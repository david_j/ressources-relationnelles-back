const express = require('express');
const router = express.Router();
const chatController = require('../controllers/chatController');
const auth = require('../middleware/auth');

// ROUTES WITHOUT PARAMETERS
router.get('/chat/help/by-current-user', auth, chatController.findAllHelpMessagesByCurrentUser);    // FIND ALL CHAT MESSAGES BY CURRENT USER
router.get('/chat/by-current-user', auth, chatController.findAllByCurrentUser);    // FIND ALL CHAT MESSAGES BY CURRENT USER
router.post('/chat', auth, chatController.sendChatMessage);    // SEND CHAT MESSAGE TO ADMIN
router.post('/chat/help', auth, chatController.sendChatHelpMessageToAdmin);    // SEND CHAT MESSAGE TO ADMIN

router.post('/chat/mark-read', auth, chatController.markChatMessagesAsRead);    // MARK CHAT MESSAGES AS READ
router.get('/chat/notifications', auth, chatController.getChatNotifications);    // GET CHAT NOTIFICATIONS

module.exports = router;
