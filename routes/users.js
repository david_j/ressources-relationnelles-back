const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../middleware/auth');

// ROUTES WITHOUT PARAMETERS
router.get('/users', auth, userController.findAll);                 // FIND ALL USERS
router.get('/users/current', auth, userController.getCurrent);      // GET CURRENT USER
router.put('/users/current', auth, userController.updateCurrent);   // UPDATE CURRENT USER
router.post('/users/current/upload-profile-picture', auth, userController.uploadProfilePicture);   // UPDATE CURRENT USER

router.get('/users/current/settings', auth, userController.findSettingsForCurrentUser);   // UPDATE CURRENT USER SETTINGS
router.post('/users/current/settings', auth, userController.updateSettingsForCurrentUser);   // UPDATE CURRENT USER SETTINGS

// ROUTES INCLUDING PARAMETERS //
router.get('/users/:id', auth, userController.findById); // FIND USER BY ID
router.post('/users/search', auth, userController.findByTextSearch); // FIND USER BY TEXT SEARCH


module.exports = router;
