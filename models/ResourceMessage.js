let mongoose = require('mongoose');

let ressourceMessageSchema = new mongoose.Schema({
    user: {type: mongoose.Types.ObjectId, ref: 'users', immutable: true},
    resource: {type: mongoose.Types.ObjectId, ref: 'resources', immutable: true},
    responseToMessage: {type: mongoose.Types.ObjectId, ref: 'resource_messages', immutable: true},
    message: String,
    isResponse: {type: Boolean, default: false},
    createdAt: {type: Date, default: Date.now(), immutable: true},
    editedAt: {type: Date, default: null, immutable: true},
    deletedAt: {type: Date, default: null, immutable: true},
    isEdited: {type: Boolean, default: false, immutable: true},
    isDeleted: {type: Boolean, default: false, immutable: true}
});

let RessourceMessage = mongoose.model('resource_messages', ressourceMessageSchema);

module.exports = RessourceMessage;
