let mongoose = require('mongoose');
const EventResponseEnum = require("../enums/EventResponse.enum").EventResponseEnum;

let eventResponseSchema = new mongoose.Schema({
    event: {type: mongoose.Types.ObjectId, ref: 'events', immutable: true},
    user: {type: mongoose.Types.ObjectId, ref: 'users', immutable: true},
    response: {
        type: String,
        enum: Object.keys(EventResponseEnum),
        default: 'UNSURE'
    },
    createdAt: {type: Date, default: Date.now(), immutable: true},
    editedAt: {type: Date, default: null, immutable: true},
    deletedAt: {type: Date, default: null, immutable: true},
    isEdited: {type: Boolean, default: false, immutable: true},
    isDeleted: {type: Boolean, default: false, immutable: true}
});

let EventResponse = mongoose.model('event_response', eventResponseSchema);

module.exports = EventResponse;
