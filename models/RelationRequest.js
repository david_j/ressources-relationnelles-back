let mongoose = require('mongoose');

let relationRequestSchema = new mongoose.Schema({
        fromUser: {type: mongoose.Types.ObjectId, ref: 'users', immutable: true},
        toUser: {type: mongoose.Types.ObjectId, ref: 'users', immutable: true},
        status: {type: String, default: 'PENDING'},
        isAnswered: {type: Boolean, default: false},
        isCancelled: {type: Boolean, default: false},
        createdAt: {type: Date, default: Date.now(), immutable: true},
        editedAt: {type: Date, default: null},
        deletedAt: {type: Date, default: null},
        isEdited: {type: Boolean, default: false},
        isDeleted: {type: Boolean, default: false}
    });

let RelationRequest = mongoose.model('relation_requests', relationRequestSchema);

module.exports = RelationRequest;
