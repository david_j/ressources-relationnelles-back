let mongoose = require('mongoose');

let chatMessageSchema = new mongoose.Schema({
    fromUser: {type: mongoose.Types.ObjectId, ref: 'users', immutable: true},
    toUser: {type: mongoose.Types.ObjectId, ref: 'users', immutable: true},
    responseToMessage: {type: mongoose.Types.ObjectId, ref: 'chat_messages', immutable: true},
    reason: {type: String, default: null},
    message: {type: String, default: null},
    isResponse: {type: Boolean, default: false},
    isFromHelpChat: {type: Boolean, default: false},
    isRead: {type: Boolean, default: false},
    createdAt: {type: Date, default: Date.now(), immutable: true},
    editedAt: {type: Date, default: null, immutable: true},
    deletedAt: {type: Date, default: null, immutable: true},
    isEdited: {type: Boolean, default: false, immutable: true},
    isDeleted: {type: Boolean, default: false, immutable: true}
});

let ChatMessage = mongoose.model('chat_messages', chatMessageSchema);

module.exports = ChatMessage;
