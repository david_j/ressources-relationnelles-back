let mongoose = require('mongoose');
const ReactionTypeEnum = require('../enums/ReactionType.enum');

let reactionSchema = new mongoose.Schema({
    user: {type: mongoose.Types.ObjectId, ref: 'users', immutable: true},
    resource: {type: mongoose.Types.ObjectId, ref: 'resources', immutable: true},
    resourceMessage: {type: mongoose.Types.ObjectId, ref: 'resource_messages', immutable: true},
    reactionType: {
        type: String,
        enum: Object.keys(ReactionTypeEnum),
        default: 'LIKE'
    },
    createdAt: {type: Date, default: Date.now(), immutable: true},
    editedAt: {type: Date, default: null, immutable: true},
    deletedAt: {type: Date, default: null, immutable: true},
    isEdited: {type: Boolean, default: false, immutable: true},
    isDeleted: {type: Boolean, default: false, immutable: true}
});

let Reaction = mongoose.model('reactions', reactionSchema);

module.exports = Reaction;
