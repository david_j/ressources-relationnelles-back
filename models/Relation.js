let mongoose = require('mongoose');

let relationSchema = new mongoose.Schema({
        user: {type: mongoose.Types.ObjectId, ref: 'users'},
        relationUser: {type: mongoose.Types.ObjectId, ref: 'users'},
        createdAt: {type: Date, default: Date.now, immutable: true},
        editedAt: {type: Date, default: null},
        deletedAt: {type: Date, default: null},
        isEdited: {type: Boolean, default: false},
        isDeleted: {type: Boolean, default: false}
    });

let Relation = mongoose.model('relations', relationSchema);

module.exports = Relation;
