let mongoose = require('mongoose');

let userSettingSchema = new mongoose.Schema({
    user: {type: mongoose.Types.ObjectId, immutable: true, unique: true, required: true},
    receiveNewsEmails: {type: Boolean, default: true},
    publicProfile: {type: Boolean, default: true}
})

let UserSettings = mongoose.model('user_settings', userSettingSchema);

module.exports = UserSettings;
