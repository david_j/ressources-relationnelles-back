let mongoose = require('mongoose');

let resourceSchema = new mongoose.Schema({
    user: {type: mongoose.Types.ObjectId, ref: 'users', immutable: true},
    type: {type: String, default: 'default'},
    event: {type: mongoose.Types.ObjectId, ref: 'events', immutable: true},
    title: String,
    message: String,
    imageUrl: String,
    image: String,
    linkUrl: String,
    messages: [{type: mongoose.Types.ObjectId, ref: 'resource_messages', immutable: true}],
    likes: [{type: mongoose.Types.ObjectId, ref: 'users'}],
    fromPosition: String,
    isBanned: {type: Boolean, default: false},
    createdAt: {type: Date, default: Date.now(), immutable: true},
    editedAt: {type: Date, default: null, immutable: true},
    deletedAt: {type: Date, default: null, immutable: true},
    isEdited: {type: Boolean, default: false, immutable: true},
    isDeleted: {type: Boolean, default: false, immutable: true}
});

let Resource = mongoose.model('resources', resourceSchema);

module.exports = Resource;
