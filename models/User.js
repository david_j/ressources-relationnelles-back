let mongoose = require('mongoose');
const UserDTO = require('../dtos/UserDTO');
const RolesEnum = require('../enums/Roles.enum');

let userSchema = new mongoose.Schema({
        userSettings: {type: mongoose.Types.ObjectId, ref: 'user_settings'},
        username: String,
        email: {type: String, required: true, unique: true, immutable: true},
        firstName: String,
        lastName: String,
        birthDate: Date,
        phone: String,
        genre: String,
        address: String,
        city: String,
        postalCode: String,
        image: Buffer,
        profilePicture: String,
        password: String,
        role: {
            type: String,
            default: 'USER',
            enum: (RolesEnum.RolesNamesFromEnum),
            immutable: true
        },
        isActivated: {
            type: Boolean,
            default: false,
            immutable: true
        },
        activationKey: {
            type: String,
            immutable: true
        },
        isBanned: {type: Boolean, default: false, immutable: true},
        createdAt: {type: Date, default: Date.now(), immutable: true},
        editedAt: {type: Date, default: null, immutable: true},
        deletedAt: {type: Date, default: null, immutable: true},
        isEdited: {type: Boolean, default: false, immutable: true},
        isDeleted: {type: Boolean, default: false, immutable: true}
    }
);

userSchema.methods.toDTO = function () {
    return new UserDTO(this);
}

let User = mongoose.model('users', userSchema);


module.exports = User;
