let mongoose = require('mongoose');

let eventSchema = new mongoose.Schema({
    user: {type: mongoose.Types.ObjectId, ref: 'users', immutable: true},
    resource: {type: mongoose.Types.ObjectId, ref: 'resources', immutable: true},
    responses: [{type: mongoose.Types.ObjectId, ref: 'event_response'}],
    title: {type: String, default: null},
    message: {type: String, default: null},
    startDate: {type: Date, default: null},
    endDate: {type: Date, default: null},
    createdAt: {type: Date, default: Date.now(), immutable: true},
    editedAt: {type: Date, default: null, immutable: true},
    deletedAt: {type: Date, default: null, immutable: true},
    isEdited: {type: Boolean, default: false, immutable: true},
    isDeleted: {type: Boolean, default: false, immutable: true}
});

let Event = mongoose.model('events', eventSchema);

module.exports = Event;
